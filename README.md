# C for BE6502 computer (Single Board Computer based of 6502)

This repository contains a simple project to get a minimum C runtime with the CC65 compiler on run on a BE6502 computer (Ben Eater 6502 computer).

If you haven't seen Ben Eater's videos, I would strongly suggest you start there: [Ben Eater's 6502 project](https://eater.net/6502)

