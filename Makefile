#
# Description:
#    Make file for BE6502 Single Board Computer
#
.SILENT:

CA65=ca65
CC65=cc65
LD65=ld65
AR65=ar65

CPU_FLAG=--cpu 65C02
ARCH_FLAG=-t none

CFG=sbc.cfg

CC65_FLAGS=$(CPU_FLAG) $(ARCH_FLAG) -O
CA65_FLAGS=$(CPU_FLAG)
LD65_FLAGS=-C $(CFG)


# Base filename.
BASE=main

# Object modules.
COBJS = $(BASE).o

OBJ_CORE = crt0.o zerobss.o copydata.o zeropage.o

# S-Rec tools
#
#   sudo apt get install s-record
#
# Convert to S-rec
$(BASE).srec: $(BASE)
	echo "Generating a S-Rec file: $@ "
	cat $< | srec_cat - -bin -Address_Length=2 -obs=16 -o $@

# Build rules.
$(BASE): $(COBJS) sbc.lib
	echo [Linking  $@ from $^ ]
	$(LD65) -o $@ $(LD65_FLAGS) -m $(BASE).map $^

# Compile C sources
%.o: %.c
	echo [Compiling C: $<]
	$(CC65) $(CC65_FLAGS) -o $(@:.o=.s) $<
	$(CA65) $(CA65_FLAGS) -o $@ -l $(@:.o=.lst) $(@:.o=.s)

%.o: %.s
	echo [Compiling asm: $<]
	$(CA65) $(CA65_FLAGS) -o $@ -l $(@:.o=.lst) $<

sbc.lib: $(OBJ_CORE)
	echo [building: $@ $<]
	$(AR65) a $@ $^

.PHONY: clean
clean:
	echo [Clean]
	-$(RM) -f $(COBJS) $(BASE) $(BASE).srec *.map *.lst sbc.lib *.o $(OBJ_CORE)


